<?php
  require_once 'init.php';
  
  $helper = $fb->getRedirectLoginHelper();
  
  try {
    $accessToken = $helper->getAccessToken();
  } catch(Facebook\Exceptions\FacebookResponseException $e) {
    // When Graph returns an error
    echo 'Graph returned an error: ' . $e->getMessage();
    exit;
  } catch(Facebook\Exceptions\FacebookSDKException $e) {
    // When validation fails or other local issues
    echo 'Facebook SDK returned an error: ' . $e->getMessage();
    exit;
  }
  
  // die(var_dump($accessToken));


  if (! isset($accessToken)) {
    if ($helper->getError()) {
      header('HTTP/1.0 401 Unauthorized');
      echo "Error: " . $helper->getError() . "\n";
      echo "Error Code: " . $helper->getErrorCode() . "\n";
      echo "Error Reason: " . $helper->getErrorReason() . "\n";
      echo "Error Description: " . $helper->getErrorDescription() . "\n";
    } else {
      header('HTTP/1.0 400 Bad Request');
      echo 'Bad request';
    }
    exit;
  }
  // var_dump($accessToken->getValue());
  
  // Logged in
  // masukan token dalam variable session
  $_SESSION['fb_access_token'] =$accessToken->getValue();
  header("location: post.php");
  // $_SESSION['fb_access_token'] = (string) $accessToken;
  // echo '<h3>Access Token</h3>';
  // var_dump($accessToken->getValue());
 
  
  
  // S$_SESSION['fb_access_token'] = (string) $accessToken;
  
 